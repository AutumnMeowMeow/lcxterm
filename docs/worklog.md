The lcxterm Project Work Log
============================

June 12, 2020

Posting a new release of 0.9.1.  Several minor bugs have been found
over the last couple years, and with the release of Xterm Window
Manager (https://xtermwm.sourceforge.io) it would be nice to have a
tarball ready to go for LCXterm to pair with it.

February 28, 2019

RGB renders OK mostly in CONSOLE, but exposes some weird interaction
bug with ncurses in SCROLLBACK.  For now I will leave it.

February 21, 2019

I've got stubs for 24-bit RGB in.  I parse it OK in VT100, just need
to get it rendering on the screen successfully.  Right now I am
fighting ncurses' view of what is on the screen and its "insider"
knowledge that it can do things like insert spaces to move a few cells
to the right because it "knows" the intervening cells are the same
color and blank.  I'm betting that just rendering entire lines if they
have any RGB will fix that.

October 21, 2018

This project has been moved to GitLab.

April 29, 2018

I modified scrollback to dynamically allocate line length rather than
hardcode 250.  We start at 132 as the largest VT100 selected right
margin, and then increase (but never decrease) if needed as the screen
size gets wider.  For a 20000-line scrollback, this cuts memory in
half down to 33MB.  Make search_colors dynamic too, and now we have
room to add back RGB colors.

Which leaves open the question of how exactly do I wish to handle RGB?
Obviously I will need to perform the same kind of trick as
double-width, mixing ncurses output and raw xterm sequences.  But do I
both trying to cache the screen like Jexer?  I don't think it's worth
it right now, I can just flush each line with RGB and use direct
ncurses dirty handling for those lines that aren't.  In other words,
we get RGB but at a performance and memory hit.

April 17, 2018

Been thinking about passthru, and decided that keyboard macros will
not be supported for it.  That greatly simplifies things, making
passthru only useful for file transfer, autotriggers (future release),
capture, and GPM.

My my my, passthru is looking niiiiiice.  Zmodem and Kermit autostart
smoothly integrated with xterm-256color.  (Also found a bug/loop in
Qodem's handle_resize() to retrofit after adding SIGWINCH.)

Exposed the default text color in options.  Made some screencasts,
updated the deb build.  Updated the rpm build, but not tested yet.

Looking like a really nice small gem here.  Honestly, if I had had
this in 2003 the rest of Qodem might never have been done.  It almost
exactly fills in the gaps for the Linux console.

Downgraded this rev to 0.9.  Let's get some more bug fixes on it and
bring in autotriggers before calling it 1.0.

April 16, 2018

GPM is working.  Buggy as hell when dragging.  BUT WORKING!  Jexer
with mouse support on the Linux console!

...and I might have just fixed the dragging. :)

Now on to passthru.  It is partly working, but it looks like my naive
one-byte-at-a-time "keyboard" breaks programs that expect an entire
VT100 sequence to present after a read().  Can't blame them though, I
used to think that too.  Should be pretty straightforward to change
passthru_win_getch() to grab an entire buffer of stuff, and then have
post_keystroke() buffer up and flush at the end.

April 15, 2018

LCXterm is fully separate from Qodem now.  Other than qodem_beep(),
qodem_win_getch(), qodem_read(), and qodem_write(), and similar
functions there are no other ties to Qodem, not even in docs.

The philosophy has crystallized too: LCXterm exists as a "terminal
fixer" or "Borland Sidekick for the terminal".  If everything is going
right, one ought to forget that LCXterm is even there at all.  When
something with the main terminal is broken or missing, LCXterm steps
in to fill the gap.

So this means that LCXterm also needs to behave more like modern
terminals.  Directories are now XDG: ~/.local/share/lcxterm instead of
~/.lcxterm, ~/Downloads for download dir, and ~ for upload dir.  UTF8
emulations are the default for XTERM and LINUX, with the 8-bit
versions relegated to X_8BIT and L_8BIT.

Coming up are two main features: libgpm support for the Linux console
(and putting it in Unicode automatically), and "passthru" that never
initializes ncurses unless is has to.  Passthru is IMHO the big
distinguisher for LCXterm over any other terminal tool.  Imagine
running a super-modern half-GUI'd terminal that has sixel, 24-bit
color, w3m-img support, the works, and it all works up until Zmodem
autostart kicks in, and then LCXterm's download window pops up to do
the work, and after that we are back to sixel/RGB/ranger goodness.  Or
in the 1.1 version when we get regex support the same plays out, but
now autotrigger macros kick in to send text to the remote side that
the local terminal doesn't even know about.  Or even have LCXterm
intercept the written stream, parse for function keys, and replace
them with macros IF those macros are defined.

Alright, let's wrap for now and get some sleep.

April 14, 2018

I'm thinking about what LCXterm's core philosophy is going to look
like, and how that will tie in to its relationship to Qodem.  I
already like LCXterm a lot, with its focus on just spawning a shell
and getting out of the way.  With the Windows-isms and of all the
connection methods removed, it is much easier to read the code and
just be focused on "being a terminal".

Anyway, some overall things to noodle down:

* I want the shared stuff to look as similar to Qodem as possible.  So
  VT100 and the protocols are to stay in sync.  I suppose that means
  qodem_read() and qodem_write() keep their names too.

* I want the main loop (qodem.c / main.c) and console.c to be where
  most of the differences lie.

* LCXterm feels a bit like a "tty piped" utility.  I went for a few
  minutes running it and not realizing.  Could it become a "tty pipe"
  utility, where one strings together LCXterm instances with different
  arguments?  For example, a simple autotrigger macro to convert the
  window resize command into a ioctl could make it a direct
  replacement for ptypipe.

* Continuing on the above thought, if console_process_incoming_data()
  had a "passthru" mode where it didn't run characters through the
  emulator but just sent them raw, but still monitored for Zmodem
  autostart, we would have a drop-in "give me Zmodem but don't fuck
  with anything else" utility.  Screen setup/teardown could be moved
  into switch_state().  NOW we've got something rather unique.

* I'm feeling an API coming on here.  Plugins that do actions based on
  regex autotrigger macros, with some actions affecting the stream
  only (passthru) so one could make something like ptypipe and drop-in
  Zmodem, and other actions affecting the screen or scrollback.

April 13, 2018

I am making a new project lifted out of Qodem.  This one is basically
Qodem MINUS: help, host mode, phonebook, scripts, non-VT100/Xterm
emulations, sound, Win32, Xcurses, serial port, and non-shell
connection methods.  Then I add libgpm support, and set the Linux
console in Unicode mode (similar to unicode_start) and one will have
X10 mouse tracking on the raw Linux console.

Current qodem is 99231 lines (via 'wc').  Let's see where we can get
this down to...

   2131   10760   90828 source/codepage.c
    300     971    7931 source/codepage.h
    306    1107   10246 source/colors.c
    180     603    4340 source/colors.h
    448    1468   10137 source/common.c
    255    1054    7268 source/common.h
   2328    6430   73529 source/console.c
    118     430    3124 source/console.h
    391    1175    9626 source/dialer.c
     74     239    1713 source/dialer.h
    787    2130   20820 source/emulation.c
    196     747    5769 source/emulation.h
    601    1880   16331 source/field.c
    276     851    5934 source/field.h
   2934    8235   98487 source/forms.c
    211    1165    7560 source/forms.h
   1549    6012   52671 source/input.c
    266     891    7875 source/input.h
   6689   19902  194862 source/kermit.c
     84     381    2659 source/kermit.h
   3540    9591  105806 source/keyboard.c
     80     275    2152 source/keyboard.h
   1425    5261   46899 source/options.c
    207     702    5835 source/options.h
   2082    5981   65428 source/protocols.c
    290    1044    8075 source/protocols.h
     54     222    1674 source/qcurses.h
   1659    5489   48925 source/qodem.c
    531    1928   13412 source/qodem.h
   1347    5890   42182 source/screen.c
    571    3080   19295 source/screen.h
   2974    9706   91439 source/scrollback.c
    414    1742   11588 source/scrollback.h
    265     571    6510 source/states.c
    111     369    3099 source/states.h
   7796   22332  236725 source/vt100.c
    142     615    4283 source/vt100.h
   3071    9828   88256 source/xmodem.c
    114     554    3908 source/xmodem.h
   5575   16871  163212 source/zmodem.c
     87     402    2824 source/zmodem.h
  52459  168884 1603237 total

Odd, I really thought killing those features would have cut the line
count more.  I'm sure there are still some other bits to remove, but
not much left.

This will be a pretty nice project I think.  It is 47% less code, a
lot less noise, and a totally different feel to actual Qodem.  If
Qodem was a Swiss army knife, then LCXterm is a Bowie knife: you run
it to get that one thing that just doesn't work in your otherwise-nice
terminal.  95% of the time you wouldn't even know it is there, until
you do 'sz blah' and suddenly the Zmodem autostart brings the file
down.  Or you hit Shift-PgUp and now you can save everything you see
to a text or HTML file.  Or you are on the Linux console and want gpm
to work correctly with Jexer.

Got my github repo created, now need to clone it and get all of this
stuff in.

Seems like I can feel that lag in input too, even under xterm.  Maybe
I can finally discover what the root cause to Qodem's terrible X11
latency is.  Aaaahhh!  I FOUND IT!  refresh_handler() was aligning
console output to 1/8 second intervals, when it only needed to do that
for a flood event.  LCXterm are Qodem and both about to get a fuckton
nicer...
