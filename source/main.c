/*
 * main.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "qcurses.h"
#include "common.h"

#include <stdlib.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <pwd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "main.h"
#include "screen.h"
#include "protocols.h"
#include "console.h"
#include "keyboard.h"
#include "states.h"
#include "options.h"
#include "shell.h"
#include "getopt.h"
#include "forms.h"

/* Set this to a not-NULL value to enable debug log. */
/* static const char * DLOGNAME = "lcxterm"; */
static const char * DLOGNAME = NULL;

/*
 * If set, enable debugging in the SIGCHLD handler.  This may lead to a hang
 * when calling select() because one should not call printf() in a signal
 * handler.
 */
/* #define DEBUG_SIGCHLD 1 */
#undef DEBUG_SIGCHLD

/*
 * Define this to enable random line noise on both sides of the link.  There
 * is a random chance that 1 in every line_noise_per_bytes/2 is turned into
 * crap.
 */
/* #define LINE_NOISE 1 */
#undef LINE_NOISE
#ifdef LINE_NOISE
static const int line_noise_per_bytes = 10000;
static Q_BOOL noise_stop = Q_FALSE;
#endif /* LINE_NOISE */

/* Global exit return code */
static int q_exitrc = EXIT_OK;

/**
 * The TTY name of the child TTY.
 */
char * q_child_ttyname = NULL;

/**
 * The child TTY descriptor.  For POSIX, this is the same descriptor for
 * command line programs, network connections, and serial port.  For Windows,
 * this is only for network connections.
 */
int q_child_tty_fd = -1;

/**
 * The child process ID.
 */
pid_t q_child_pid = -1;

/**
 * If true, we have received a SIGCHLD that matches q_child_pid.
 */
Q_BOOL q_child_exited = Q_FALSE;

/**
 * Global status struct.
 */
struct q_status_struct q_status;

/**
 * The physical screen width.
 */
int WIDTH;

/**
 * The physical screen height.
 */
int HEIGHT;

/**
 * The height of the status bar.  Currently this is either 0 or 1, but in the
 * future it could become several lines.
 */
int STATUS_HEIGHT;

/**
 * The base working directory where LCXterm stores its config files.  This is
 * usually ~/.lcxterm.
 */
char * q_home_directory = NULL;

/*
 * The input buffer for raw bytes seen from the remote side.
 */
static unsigned char q_buffer_raw[Q_BUFFER_SIZE];
static int q_buffer_raw_n = 0;

/*
 * The output buffer used by qodem_buffered_write() and
 * qodem_buffered_write_flush().
 */
static char * buffered_write_buffer = NULL;
static int buffered_write_buffer_i = 0;
static int buffered_write_buffer_n = 0;

/*
 * The output buffer for sending raw bytes to the remote side.  This is used
 * by the modem dialer, scripts, host mode, and file transfer protocols.
 * Console (keyboard) output does not use this, that is sent directly to
 * qodem_write().
 */
static unsigned char q_transfer_buffer_raw[Q_BUFFER_SIZE];
static unsigned int q_transfer_buffer_raw_n = 0;

/* These are used by the select() call in data_handler() */
static fd_set readfds;
static fd_set writefds;
static fd_set exceptfds;

/* The --status-line command line argument */
static Q_BOOL status_line_visible = Q_FALSE;

/**
 * The --keyfile command line argument.
 */
char * q_keyfile = NULL;

/**
 * The --config command line argument.
 */
char * q_config_filename = NULL;

/**
 * The --dotlcxterm-dir command line argument.
 */
char * q_dotlcxterm_dir = NULL;

/**
 * The --codepage command line argument.
 */
static char * q_codepage_option = NULL;

/**
 * The --doorway command line argument.
 */
static char * q_doorway_option = NULL;

/**
 * The --emulation command line argument.
 */
static char * q_emulation_option = NULL;

/**
 * The --capfile command line argument.
 */
static char * q_capfile_option = NULL;

/* Command-line options */
static struct option q_getopt_long_options[] = {
    {"dial",                1,      0,      0},
    {"connect",             1,      0,      0},
    {"connect-method",      1,      0,      0},
    {"capfile",             1,      0,      0},
    {"keyfile",             1,      0,      0},
    {"config",              1,      0,      0},
    {"create-config",       1,      0,      0},
    {"dotlcxterm-dir",      1,      0,      0},
    {"read-only",           0,      0,      0},
    {"help",                0,      0,      0},
    {"version",             0,      0,      0},
    {"doorway",             1,      0,      0},
    {"codepage",            1,      0,      0},
    {"emulation",           1,      0,      0},
    {"status-line",         1,      0,      0},
    {"passthru",            0,      0,      0},
    {0,                     0,      0,      0}
};

/**
 * The filename returned by get_datadir_filename(),
 * get_workingdir_filename(), and get_scriptdir_filename().
 */
static char datadir_filename[FILENAME_SIZE];

/**
 * If true, SIGWINCH was received and the child process needs its screen size
 * updated.
 */
static Q_BOOL need_screen_resize = Q_FALSE;

/**
 * SIGCHLD signal handler.
 *
 * @param sig the signal number
 */
static void handle_sigchld(int sig) {
    pid_t pid;
    int status;

#ifdef DEBUG_SIGCHLD
    DLOG(("handle_sigchld() ENTER\n"));
#endif

    if (q_child_pid == -1) {
        /*
         * We got SIGCHLD, but think we are offline anyway.  Just say we
         * exited.
         */
        q_child_exited = Q_TRUE;
    }

    for (;;) {
#ifdef DEBUG_SIGCHLD
        DLOG(("handle_sigchld() calling waitpid()...\n"));
#endif
        pid = waitpid(-1, &status, WNOHANG);
        if (pid == -1) {
            /*
             * Error in the waitpid() call.
             */
#ifdef DEBUG_SIGCHLD
            DLOG(("Error in waitpid(): %s (%d)\n", strerror(errno), errno));
#endif
            break;
        }
        if (pid == 0) {
            /*
             * Reaped the last zombie, bail out now.
             */
#ifdef DEBUG_SIGCHLD
            DLOG(("No more zombies\n"));
#endif
            break;
        }
        if (pid == q_child_pid) {
            /*
             * The connection has closed.
             */
            q_child_exited = Q_TRUE;
#ifdef DEBUG_SIGCHLD
            DLOG(("SIGCHLD: CONNECTION CLOSED\n"));
#endif
        }

#ifdef DEBUG_SIGCHLD
        DLOG(("Reaped process %d\n", pid));
#endif
    }
}

/**
 * SIGWINCH signal handler.
 *
 * @param sig the signal number
 */
static void handle_sigwinch(int sig) {
    need_screen_resize = Q_TRUE;
}

/**
 * Write data from a buffer to the remote system, dispatching to the
 * appropriate connection-specific write function.
 *
 * @param fd the socket descriptor
 * @param data the buffer to read from
 * @param data_n the number of bytes to write to the remote side
 * @param sync if true, do not return until all of the bytes have been
 * written, performing a busy wait and retry.
 * @return the number of bytes written
 */
int qodem_write(const int fd, const char * data, const int data_n,
                const Q_BOOL sync) {

    int i;
    int rc;
    int old_errno;
    int begin = 0;
    int n = data_n;

    /*
     * We were passed a const data so that callers can pass read-only string
     * literals, but might need to change data before it hits the wire.
     */
    char write_buffer_data[Q_BUFFER_SIZE];
    char * write_buffer = (char *) data;

    if (data_n == 0) {
        /* NOP */
        return 0;
    }

    if (sync == Q_TRUE) {
        DLOG(("qodem_write() SYNC is TRUE\n"));

        /*
         * Every caller that syncs is sending data in console mode: emulation
         * responses, modem command strings, and keystrokes.  The only caller
         * that doesn't sync is process_incoming_data().  We run bytes
         * through the 8-bit translate table here and in
         * process_incoming_data() so that everything is converted only once.
         */
        write_buffer = write_buffer_data;
        for (i = 0; i < data_n; i++) {
            write_buffer_data[i] = data[i];
        }
    }

    if (DLOGNAME != NULL) {

        DLOG(("qodem_write() OUTPUT bytes: "));
        for (i = 0; i < data_n; i++) {
            DLOG2(("%02x ", write_buffer[i] & 0xFF));
        }
        DLOG2(("\n"));
        DLOG(("qodem_write() OUTPUT bytes (ASCII): "));
        for (i = 0; i < data_n; i++) {
            DLOG2(("%c ", write_buffer[i] & 0xFF));
        }
        DLOG2(("\n"));
    }

do_write:

    /* Write bytes out */
    rc = write(fd, write_buffer + begin, data_n);

    old_errno = errno;
    if (rc < 0) {
        DLOG(("qodem_write() write() error %s (%d)\n", strerror(old_errno),
                old_errno));
    } else if (rc == 0) {
        DLOG(("qodem_write() write() RC=0\n"));

        if (sync == Q_TRUE) {
            DLOG(("qodem_write() write() RC=0 SYNC is true, go back\n"));
            goto do_write;
        }
    } else {
        DLOG(("qodem_write() write() %d bytes written\n", rc));
    }

    if (sync == Q_TRUE) {
        int error = errno;
        if (rc > 0) {
            n -= rc;
            begin += n;
            if (n > 0) {
                /*
                 * The last write was successful, and there are more bytes to
                 * write.
                 */
                goto do_write;
            }
        } else if ((rc == 0) || (error == EAGAIN) ||
            (error == EWOULDBLOCK)
        ) {
            /*
             * Do a busy wait on the write until everything goes out.
             */
            goto do_write;
        }

    }

    /* Reset errno for our caller */
    errno = old_errno;

    /* All done */
    return rc;
}

/**
 * Buffer up data to write to the remote system.
 *
 * @param data the buffer to read from
 * @param data_n the number of bytes to write to the remote side
 */
void qodem_buffered_write(const char * data, const int data_n) {
    if (DLOGNAME != NULL) {
        int i;

        DLOG(("qodem_buffered_write() OUTPUT bytes: "));
        for (i = 0; i < data_n; i++) {
            DLOG2(("%02x ", data[i] & 0xFF));
        }
        DLOG2(("\n"));
        DLOG(("qodem_buffered_write() OUTPUT bytes (ASCII): "));
        for (i = 0; i < data_n; i++) {
            DLOG2(("%c ", data[i] & 0xFF));
        }
        DLOG2(("\n"));
    }

    if (buffered_write_buffer == NULL) {
        assert(buffered_write_buffer_n == 0);
        buffered_write_buffer = (char *) Xmalloc(sizeof(char) * data_n,
            __FILE__, __LINE__);
        buffered_write_buffer_n += data_n;
    } else if ((buffered_write_buffer_i + data_n) > buffered_write_buffer_n) {
        buffered_write_buffer = (char *) Xrealloc(buffered_write_buffer,
            sizeof(char) * (buffered_write_buffer_n + data_n), __FILE__,
            __LINE__);
        buffered_write_buffer_n += data_n;
    } else {
        assert(buffered_write_buffer_i + data_n <= buffered_write_buffer_n);
    }

    memcpy(buffered_write_buffer + buffered_write_buffer_i, data, data_n);
    buffered_write_buffer_i += data_n;
}

/**
 * Write data from the buffer of qodem_buffered_write() to the remote system,
 * dispatching to the appropriate connection-specific write function.
 *
 * @param fd the socket descriptor
 */
void qodem_buffered_write_flush(const int fd) {
    DLOG(("qodem_buffered_write_flush()\n"));

    if (buffered_write_buffer_i > 0) {
        qodem_write(fd, buffered_write_buffer, buffered_write_buffer_i, Q_TRUE);
    } else {
        assert(buffered_write_buffer_i == 0);
    }
    buffered_write_buffer_i = 0;
}

/**
 * Read data from remote system to a buffer, dispatching to the
 * appropriate connection-specific read function.
 *
 * @param fd the socket descriptor
 * @param buf the buffer to write to
 * @param count the number of bytes requested
 * @return the number of bytes read into buf
 */
static ssize_t qodem_read(const int fd, void * buf, size_t count) {
    return read(fd, buf, count);
}

/**
 * Get the command-line help screen.
 *
 * @return the help screen
 */
static char * usage_string() {
        return _(""
"'lcxterm' is a terminal emulator with support for scrollback, capture, file\n"
"transfers, keyboard macros, and more.  This is version 0.9.1.\n"
"\n"
"Usage: lcxterm [OPTIONS] {\n"
"\n"
"Options:\n"
"\n"
"      --dotlcxterm-dir DIRNAME    Use DIRNAME instead of\n"
"                                  $HOME/.local/share/lcxterm for config/data\n"
"                                  files.\n"
"      --config FILENAME           Load options from FILENAME (only).\n"
"      --create-config FILENAME    Write a new options file to FILENAME and exit.\n"
"      --capfile FILENAME          Capture the entire session and save to\n"
"                                  FILENAME.\n"
"      --keyfile FILENAME          Load keyboard macros from FILENAME\n"
"  -p, --passthru                  Be transparent to terminal (no ncurses).\n"
"      --read-only                 Disable all writes to disk.\n"
"      --doorway MODE              Select doorway MODE.  Valid values for\n"
"                                  MODE are \"doorway\", \"mixed\", and \"off\".\n"
"      --codepage CODEPAGE         Select codepage CODEPAGE.  See Alt-; list\n"
"                                  for valid codepages.  Example: \"CP437\",\n"
"                                  \"CP850\", \"Windows-1252\", etc.\n"
"      --emulation EMULATION       Select emulation EMULATION.  Valid values are\n"
"                                  \"vt100\", \"vt102\", \"vt220\", \"l_8bit\",\n"
"                                  \"linux\", \"x_8bit\", and \"xterm\".\n"
"      --status-line { on | off }  If \"on\" enable status line.  If \"off\" disable\n"
"                                  status line.\n"
"      --version                   Display program version\n"
"  -h, --help                      This help screen\n"
"\n"
"\n");
}

/**
 * Get the version string
 *
 * @return the version string
 */
static char * version_string() {
    return _(""
"lcxterm version 0.9.1\n"
"Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now\n"
"\n"
"To the extent possible under law, the author(s) have dedicated all\n"
"copyright and related and neighboring rights to this software to the\n"
"public domain worldwide. This software is distributed without any\n"
"warranty.\n"
"\n");
}

/**
 * Display a multi-line string to the user.  This is used to emit the help
 * and version text.
 *
 * @param str the string
 */
static void page_string(const char * str) {
    printf("%s", str);
}

/**
 * See if the user asked for help or version information, and if so provide a
 * return code to main().
 *
 * @param argc the program's command line argument count
 * @param argc the program's command line arguments
 * @return an exit code if the user asked for help or version, or 0 for main
 * to continue
 */
static int check_for_help(int argc, char * const argv[]) {
    int i;

    for (i = 0; i < argc; i++) {

        /* Special case: help means exit */
        if (strncmp(argv[i], "--help", strlen("--help")) == 0) {
            page_string(usage_string());
            return EXIT_HELP;
        }
        if (strncmp(argv[i], "-h", strlen("-h")) == 0) {
            page_string(usage_string());
            return EXIT_HELP;
        }
        if (strncmp(argv[i], "-?", strlen("-?")) == 0) {
            page_string(usage_string());
            return EXIT_HELP;
        }
        if (strncmp(argv[i], "--version", strlen("--version")) == 0) {
            page_string(version_string());
            return EXIT_VERSION;
        }
    }

    /* The user did not ask for help or version */
    return 0;
}

/**
 * Process one command line option.
 *
 * @param option the option, e.g. "play"
 * @param value the value for the option, e.g. "M BCEBCEBCE"
 */
static void process_command_line_option(const char * option,
                                        const char * value) {

    /*
     fprintf(stdout, "OPTION=%s VALUE=%s\n", option, value);
     */

    /* Special case: help means exit */
    if (strncmp(option, "help", strlen("help")) == 0) {
        page_string(usage_string());
        q_program_state = Q_STATE_EXIT;
    }

    /* Special case: version means exit */
    if (strncmp(option, "version", strlen("version")) == 0) {
        page_string(version_string());
        q_program_state = Q_STATE_EXIT;
    }

    if (strncmp(option, "keyfile", strlen("keyfile")) == 0) {
        q_keyfile = Xstrdup(value, __FILE__, __LINE__);
    }

    if (strncmp(option, "config", strlen("config")) == 0) {
        q_config_filename = Xstrdup(value, __FILE__, __LINE__);
    }
    if (strncmp(option, "create-config", strlen("create-config")) == 0) {
        reset_options();
        save_options(value);
        q_program_state = Q_STATE_EXIT;
    }
    if (strncmp(option, "dotlcxterm-dir", strlen("dotlcxterm-dir")) == 0) {
        q_dotlcxterm_dir = Xstrdup(value, __FILE__, __LINE__);
    }
    if (strncmp(option, "read-only", strlen("read-only")) == 0) {
        q_status.read_only = Q_TRUE;
    }
    if (strncmp(option, "passthru", strlen("passthru")) == 0) {
        q_status.passthru = Q_TRUE;
    }

    if (strncmp(option, "doorway", strlen("doorway")) == 0) {
        q_doorway_option = Xstrdup(value, __FILE__, __LINE__);
    }

    if (strncmp(option, "codepage", strlen("codepage")) == 0) {
        q_codepage_option = Xstrdup(value, __FILE__, __LINE__);
    }

    if (strncmp(option, "emulation", strlen("emulation")) == 0) {
        q_emulation_option = Xstrdup(value, __FILE__, __LINE__);
    }

    if (strncmp(option, "status-line", strlen("status-line")) == 0) {
        if (strcasecmp(value, "off") == 0) {
            set_status_line(Q_FALSE);
            status_line_visible = Q_FALSE;
        } else {
            set_status_line(Q_TRUE);
            status_line_visible = Q_TRUE;
        }
    }

    if (strncmp(option, "capfile", strlen("capfile")) == 0) {
        q_capfile_option = Xstrdup(value, __FILE__, __LINE__);
    }

}

/**
 * Resolve conflicts between command line options and the options file.
 */
static void resolve_command_line_options() {

    if ((strncmp(get_option(Q_OPTION_STATUS_LINE_VISIBLE),
                "false", 5) == 0) &&
        (status_line_visible == Q_FALSE)
    ) {
        set_status_line(Q_FALSE);
    } else {
        set_status_line(Q_TRUE);
    }

    if (q_emulation_option != NULL) {
        q_status.emulation = emulation_from_string(q_emulation_option);
        q_status.codepage = default_codepage(q_status.emulation);
    }

    if (q_codepage_option != NULL) {
        q_status.codepage = codepage_from_string(q_codepage_option);
    }

    if (q_capfile_option != NULL) {
        start_capture(q_capfile_option);
    }

}

/**
 * Close a wrapped shell connection.
 */
void close_shell_connection() {

    int status = 0;

    DLOG(("close_shell_connection()\n"));

    assert(q_child_pid != -1);
    assert(q_child_tty_fd != -1);

    /* Close pty */
    close(q_child_tty_fd);
    q_child_tty_fd = -1;
    Xfree(q_child_ttyname, __FILE__, __LINE__);
    wait4(q_child_pid, &status, WNOHANG, NULL);
    if (WIFEXITED(status)) {
        q_exitrc = WEXITSTATUS(status);
    } else if (WIFSIGNALED(status)) {
    }
    q_child_pid = -1;
}

/**
 * Cleanup connection resources, called AFTER read() has returned 0.
 */
static void cleanup_connection() {

    DLOG(("cleanup_connection()\n"));

    close_shell_connection();

    /* Offline now */
    q_status.online = Q_FALSE;

    /* Automatically exit */
    q_program_state = Q_STATE_EXIT;
}

/**
 * Kill the local shell.
 */
void kill_shell() {

    DLOG(("kill_shell()\n"));

    /* Killing -1 kills EVERYTHING.  Not good! */
    assert(q_child_pid != -1);
    kill(q_child_pid, SIGHUP);
}

/**
 * See if the fd is readable.
 *
 * @return true if the fd is readable
 */
static Q_BOOL is_readable(int fd) {

    if (FD_ISSET(fd, &readfds)) {
        return Q_TRUE;
    }
    return Q_FALSE;
}

/**
 * Read data from the remote side, dispatch it to the correct data handling
 * function, and write data to the remote side.
 */
static void process_incoming_data() {
    int rc;
    int n;
    int unprocessed_n;
    char time_string[SHORT_TIME_SIZE];
    time_t current_time;
    int hours, minutes, seconds;
    time_t connect_time;
    int i;
    char notify_message[DIALOG_MESSAGE_SIZE];

    DLOG(("IF CHECK: %s %s\n",
            (q_status.online == Q_TRUE ? "true" : "false"),
            (is_readable(q_child_tty_fd) == Q_TRUE ? "true" : "false")));

    if ((q_status.online == Q_TRUE) &&
        (is_readable(q_child_tty_fd) == Q_TRUE)
    ) {

        /*
         * There is something to read.
         */
        n = Q_BUFFER_SIZE - q_buffer_raw_n;

        DLOG(("before qodem_read(), n = %d\n", n));

        if (n > 0) {
            int error;

            /* Clear errno */
            errno = 0;
            rc = qodem_read(q_child_tty_fd, q_buffer_raw + q_buffer_raw_n, n);
            error = errno;

            DLOG(("qodem_read() : rc = %d errno=%d\n", rc, error));

            if (rc < 0) {
                if (error == EIO) {
                    /* This is EOF. */
                    rc = 0;
                } else if (error == ECONNRESET) {
                    /* "Connection reset by peer".  This is EOF. */
                    rc = 0;
                } else {
                    DLOG(("Call to read() failed: %d %s\n",
                            error, strerror(error)));

                    snprintf(notify_message, sizeof(notify_message),
                        _("Call to read() failed: %d (%s)"),
                        error, strerror(error));
                    notify_form(notify_message, 0);
                    /*
                     * Treat it like EOF.  This will terminate the
                     * connection.
                     */
                    rc = 0;
                }
            } /* if (rc < 0) */

            if (rc == 0) {
                /* EOF */
                /* All other */
                cleanup_connection();

                /* Compute time */
                /* time_string needs to be hours/minutes/seconds CONNECTED */
                time(&current_time);
                connect_time = (time_t) difftime(current_time,
                                                 q_status.connect_time);
                hours   = (int)  (connect_time / 3600);
                minutes = (int) ((connect_time % 3600) / 60);
                seconds = (int)  (connect_time % 60);
                snprintf(time_string, sizeof(time_string), "%02u:%02u:%02u",
                    hours, minutes, seconds);

                return;
            }

#ifdef LINE_NOISE
            for (i = 0; i < rc; i++) {
                int do_noise = random() % line_noise_per_bytes;
                if ((do_noise == 1) && (noise_stop == Q_FALSE)) {
                    q_buffer_raw[i] = random() % 0xFF;
                    noise_stop = Q_TRUE;
                    break;
                }
            }
#endif

            /* Record # of new bytes in */
            q_buffer_raw_n += rc;

            if (DLOGNAME != NULL) {
                DLOG(("INPUT bytes: "));
                for (i = 0; i < q_buffer_raw_n; i++) {
                    DLOG2(("%02x ", q_buffer_raw[i] & 0xFF));
                }
                DLOG2(("\n"));
                DLOG(("INPUT bytes (ASCII): "));
                for (i = 0; i < q_buffer_raw_n; i++) {
                    DLOG2(("%c ", q_buffer_raw[i] & 0xFF));
                }
                DLOG2(("\n"));
            }
        } /* if (n > 0) */
    } /* if (FD_ISSET(q_child_tty_fd, &readfds)) */

    if (DLOGNAME != NULL) {
        DLOG(("\n"));
        DLOG(("q_program_state: "));
        switch (q_program_state) {
        case Q_STATE_CONSOLE:
            DLOG2(("Q_STATE_CONSOLE"));
            break;
#ifndef Q_NO_PROTOCOLS
        case Q_STATE_UPLOAD:
            DLOG2(("Q_STATE_UPLOAD"));
            break;
        case Q_STATE_DOWNLOAD:
            DLOG2(("Q_STATE_DOWNLOAD"));
            break;
        case Q_STATE_UPLOAD_BATCH:
            DLOG2(("Q_STATE_UPLOAD_BATCH"));
            break;
#endif
        default:
            DLOG2(("%d", q_program_state));
            break;
        }
        DLOG2((" q_transfer_buffer_raw_n %d\n", q_program_state, q_transfer_buffer_raw_n));
        if (q_transfer_buffer_raw_n > 0) {
            DLOG(("LEFTOVER OUTPUT\n"));
        }
        DLOG(("\n"));
    }

    unprocessed_n = q_buffer_raw_n;

#ifndef Q_NO_PROTOCOLS

    if ((q_program_state == Q_STATE_UPLOAD) ||
        (q_program_state == Q_STATE_UPLOAD_BATCH) ||
        (q_program_state == Q_STATE_DOWNLOAD)
    ) {
        /*
         * File transfers, scripts, and host mode: run
         * protocol_process_data() until old_q_transfer_buffer_raw_n ==
         * q_transfer_buffer_raw_n .
         *
         * Every time we come through process_incoming_data() we call
         * protocol_process_data() at least once.
         */

        int old_q_transfer_buffer_raw_n = -1;
        DLOG(("ENTER TRANSFER LOOP\n"));

        while (old_q_transfer_buffer_raw_n != q_transfer_buffer_raw_n) {
            unprocessed_n = q_buffer_raw_n;
            old_q_transfer_buffer_raw_n = q_transfer_buffer_raw_n;

            DLOG(("2 old_q_transfer_buffer_raw_n %d q_transfer_buffer_raw_n %d unprocessed_n %d\n",
                    old_q_transfer_buffer_raw_n, q_transfer_buffer_raw_n,
                    unprocessed_n));

            if ((q_program_state == Q_STATE_UPLOAD) ||
                (q_program_state == Q_STATE_UPLOAD_BATCH) ||
                (q_program_state == Q_STATE_DOWNLOAD)
            ) {
                /* File transfer protocol data handler */
                protocol_process_data(q_buffer_raw, q_buffer_raw_n,
                    &unprocessed_n, q_transfer_buffer_raw,
                    &q_transfer_buffer_raw_n, sizeof(q_transfer_buffer_raw));
            }

            DLOG(("3 old_q_transfer_buffer_raw_n %d q_transfer_buffer_raw_n %d unprocessed_n %d\n",
                    old_q_transfer_buffer_raw_n, q_transfer_buffer_raw_n,
                    unprocessed_n));

            /* Hang onto whatever was unprocessed */
            if (unprocessed_n > 0) {
                memmove(q_buffer_raw, q_buffer_raw +
                    q_buffer_raw_n - unprocessed_n, unprocessed_n);
            }
            q_buffer_raw_n = unprocessed_n;

            DLOG(("4 old_q_transfer_buffer_raw_n %d q_transfer_buffer_raw_n %d unprocessed_n %d\n",
                    old_q_transfer_buffer_raw_n, q_transfer_buffer_raw_n,
                    unprocessed_n));

        }

        DLOG(("EXIT TRANSFER LOOP\n"));

    }

#endif /* Q_NO_PROTOCOLS */

    /* Terminal mode */
    if (q_program_state == Q_STATE_CONSOLE) {
        DLOG(("console_process_incoming_data: > q_buffer_raw_n %d unprocessed_n %d\n",
                q_buffer_raw_n, unprocessed_n));

        /*
         * Usability issue: if we're in the middle of a very fast but botched
         * download, the keyboard will become hard to use if we keep reading
         * stuff and processing through the console.  So flag a potential
         * console flood.
         */
        if (q_program_state == Q_STATE_CONSOLE) {
            if (q_buffer_raw_n >= sizeof(q_buffer_raw) / 2) {
                q_console_flood = Q_TRUE;
            } else {
                q_console_flood = Q_FALSE;
            }
        }

        /* Let the console process the data */
        console_process_incoming_data(q_buffer_raw, q_buffer_raw_n,
            &unprocessed_n);

        DLOG(("console_process_incoming_data: < q_buffer_raw_n %d unprocessed_n %d\n",
                q_buffer_raw_n, unprocessed_n));
    }

    assert(q_transfer_buffer_raw_n >= 0);
    assert(unprocessed_n >= 0);

    /* Hang onto whatever was unprocessed */
    if (unprocessed_n > 0) {
        memmove(q_buffer_raw, q_buffer_raw + q_buffer_raw_n - unprocessed_n,
            unprocessed_n);
    }
    q_buffer_raw_n = unprocessed_n;

    DLOG(("online = %s q_transfer_buffer_raw_n = %d\n",
            (q_status.online == Q_TRUE ? "true" : "false"),
            q_transfer_buffer_raw_n));

    /* Write the data in the output buffer to q_child_tty_fd */
    if ((q_status.online == Q_TRUE) && (q_transfer_buffer_raw_n > 0)) {

#ifdef LINE_NOISE
        for (i = 0; i < q_transfer_buffer_raw_n; i++) {
            int do_noise = random() % line_noise_per_bytes;
            if ((do_noise == 1) && (noise_stop == Q_FALSE)) {
                q_transfer_buffer_raw[i] = random() % 0xFF;
                noise_stop = Q_TRUE;
                break;
            }
        }
#endif

        /*
         * Write the data to q_child_tty_fd.
         */
        rc = qodem_write(q_child_tty_fd, (char *) q_transfer_buffer_raw,
            q_transfer_buffer_raw_n, Q_FALSE);

        if (rc < 0) {
            int error = errno;

            switch (error) {

            case EAGAIN:
                /*
                 * Outgoing buffer is full, wait for the next round
                 */
                break;
            default:
                DLOG(("Call to write() failed: %d %s\n",
                        error, strerror(error)));

                /* Uh-oh, error */
                snprintf(notify_message, sizeof(notify_message),
                    _("Call to write() failed: %s"), strerror(error));
                notify_form(notify_message, 0);
                return;
            }
        } else {

            DLOG(("%d bytes written\n", rc));

            /* Hang onto the difference for the next round */
            assert(rc <= q_transfer_buffer_raw_n);

            if (rc < q_transfer_buffer_raw_n) {
                memmove(q_transfer_buffer_raw, q_transfer_buffer_raw + rc,
                    q_transfer_buffer_raw_n - rc);
            }
            q_transfer_buffer_raw_n -= rc;
        }
    }
}

/**
 * See if a child process has exited.  For non-shell connections, this
 * returns false.
 *
 * @return true if the child process has exited
 */
static Q_BOOL child_is_dead() {
    /* POSIX case */
    if (q_child_pid == -1) {
        /*
         * This is not a shell connection.
         */
        return Q_FALSE;
    }

    /* See if SIGCHLD was received before. */
    return q_child_exited;
}

/**
 * Check various data sources and sinks for data, and dispatch to appropriate
 * handlers.
 */
static void data_handler() {
    int rc;
    int error;
    int select_fd_max;
    struct timeval listen_timeout;
    int default_timeout;
    char notify_message[DIALOG_MESSAGE_SIZE];
    Q_BOOL have_data = Q_FALSE;

    /*
     * For network connections through the dialer, we might have read data
     * before we got to STATE_CONSOLE.  So look at q_buffer_raw_n and set
     * have_data appropriately.
     */
    if (q_buffer_raw_n > 0) {
        have_data = Q_TRUE;
    }

    /*
     * Default is to block 20 milliseconds (50Hz).
     */
    default_timeout = 20000;

    /* Initialize select() structures */
    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);

    /* Add stdin */
    select_fd_max = STDIN_FILENO;
    FD_SET(STDIN_FILENO, &readfds);

    /* Add the child tty */
    if (q_child_tty_fd != -1) {
        /*
         * Do not read data while in some program states.
         */
        switch (q_program_state) {
#ifndef Q_NO_PROTOCOLS
        case Q_STATE_UPLOAD:
        case Q_STATE_UPLOAD_BATCH:
        case Q_STATE_DOWNLOAD:
#endif
        case Q_STATE_CONSOLE:
            DLOG(("select on q_child_tty_fd = %d\n", q_child_tty_fd));

            /* These states are OK to read() */
            FD_SET(q_child_tty_fd, &readfds);

            /* Flag if we need to send data out to the child tty */
            if (q_transfer_buffer_raw_n > 0) {
                FD_SET(q_child_tty_fd, &writefds);
            }
            if (q_child_tty_fd > select_fd_max) {
                select_fd_max = q_child_tty_fd;
            }

#if defined(__linux) && defined(Q_ENABLE_GPM)
            /*
             * If we have GPM running, select on its descriptor.
             */
            if ((q_gpm_mouse == Q_TRUE) && (gpm_fd > 2)) {
                DLOG(("GPM FD = %d\n", gpm_fd));
                FD_SET(gpm_fd, &readfds);
                if (gpm_fd > select_fd_max) {
                    select_fd_max = gpm_fd;
                }
            }
#endif
            break;
#ifndef Q_NO_PROTOCOLS
        case Q_STATE_DOWNLOAD_MENU:
        case Q_STATE_UPLOAD_MENU:
        case Q_STATE_DOWNLOAD_PATHDIALOG:
        case Q_STATE_UPLOAD_PATHDIALOG:
        case Q_STATE_UPLOAD_BATCH_DIALOG:
#endif
        case Q_STATE_EMULATION_MENU:
        case Q_STATE_INITIALIZATION:
        case Q_STATE_CODEPAGE:
        case Q_STATE_SCROLLBACK:
        case Q_STATE_CONSOLE_MENU:
#ifndef Q_NO_KEYMACROS
        case Q_STATE_FUNCTION_KEY_EDITOR:
#endif
        case Q_STATE_EXIT:
            /* For these states, do NOT read() */
            break;
        }
    }

    /* select() needs 1 + MAX */
    select_fd_max++;

    /* Set the timeout */
    listen_timeout.tv_sec = default_timeout / 1000000;
    listen_timeout.tv_usec = default_timeout % 1000000;

    /*
    DLOG(("call select(): select_fd_max = %d tv_sec %d tv_usec %d\n",
            select_fd_max, listen_timeout.tv_sec, listen_timeout.tv_usec));
    */

    /*
     * If select() hangs here, it is probably because DLOG() in the SIGCHLD
     * handler has been called, which is a windy path to printf which is not
     * safe to call in a signal handler.  Turn off debugging and be happy.
     */
    rc = select(select_fd_max, &readfds, &writefds, &exceptfds,
                &listen_timeout);

    DLOG(("q_program_state = %d select() returned %d\n", q_program_state, rc));

    switch (rc) {

    case -1:
        /* ERROR */
        error = errno;

        switch (error) {
        case EINTR:
            /*
             * Interrupted system call, say from a SIGWINCH.
             */
            if (q_keyboard_blocks == Q_FALSE) {
                keyboard_handler();
            }
            break;
        default:
            DLOG(("Call to select() failed: %d %s\n",
                    error, strerror(error)));

            snprintf(notify_message, sizeof(notify_message),
                _("Call to select() failed: %d %s"),
                error, strerror(error));
            notify_form(notify_message, 0);
            exit(EXIT_ERROR_SELECT_FAILED);
        }
        break;

    case 0:
        /*
         * We timed out looking for data.  See if other things need to run
         * during this idle period.
         */

        /* Flush capture file if necessary */
        if (q_status.capture == Q_TRUE) {
            if (q_status.capture_flush_time < time(NULL)) {
                fflush(q_status.capture_file);
                q_status.capture_flush_time = time(NULL);
            }
        }

        /*
         * See if the child process died.  It's possible for the child
         * process to be defunct but the fd still be active, e.g. if a shell
         * has a blocked background process with an open handle to its
         * stdout.
         */
        if (child_is_dead() == Q_TRUE) {
            kill_shell();

            /*
             * We need to cleanup immediately, because read() will never
             * return 0.
             */
            cleanup_connection();

            /*
             * Don't allow the loop to enter process_incoming_data().
             */
            break;
        }

        /*
         * See if there are any file transfers, scripts, or host mode to run.
         */
        if (
#ifndef Q_NO_PROTOCOLS
            (q_program_state == Q_STATE_DOWNLOAD) ||
            (q_program_state == Q_STATE_UPLOAD) ||
            (q_program_state == Q_STATE_UPLOAD_BATCH) ||
#endif
            (have_data == Q_TRUE)
        ) {

            /* Process incoming data */
            process_incoming_data();

        }

        /*
         * We either have an idle redraw, or something was processed and
         * might have changed the screen.
         */
        refresh_handler();
        break;

    default:
        /*
         * At least one descriptor is readable or writeable.
         */

        DLOG(("q_child_tty %s %s %s\n",
                (FD_ISSET(q_child_tty_fd, &readfds) ? "READ" : ""),
                (FD_ISSET(q_child_tty_fd, &writefds) ? "WRITE" : ""),
                (FD_ISSET(q_child_tty_fd, &exceptfds) ? "EXCEPT" : "")));

        /*
         * Data is present somewhere, go process it.
         */
        if (((q_child_tty_fd > 0) && (is_readable(q_child_tty_fd))) ||
            ((q_child_tty_fd > 0) && (FD_ISSET(q_child_tty_fd, &writefds)))
        ) {
            /* Process incoming data */
            process_incoming_data();
        }

        /*
         * If the keyboard has data, read it.
         */
        if (FD_ISSET(STDIN_FILENO, &readfds)) {
            keyboard_handler();
        }

#if defined(__linux) && defined(Q_ENABLE_GPM)
        /*
         * If GPM has data, process it.
         */
        if ((q_gpm_mouse == Q_TRUE) && FD_ISSET(gpm_fd, &readfds)) {
            DLOG(("GPM is readable, check it...\n"));
            Gpm_Event event;
            if (Gpm_GetEvent(&event) > 0) {
                DLOG(("Gpm_GetEvent() > 0, call gpm_handle_mouse()\n"));
                gpm_handle_mouse(&event, 0);
            }
        }
#endif

        /*
         * Whatever was just processed might have updated the screen, so
         * redraw.
         */
        refresh_handler();
        break;
    }

    if (need_screen_resize == Q_TRUE) {
        assert (q_status.passthru == Q_TRUE);

        if (q_status.online == Q_TRUE) {
            /*
             * Set the child process cols and rows.
             */
            struct winsize console_size;
            if (ioctl(STDIN_FILENO, TIOCGWINSZ, &console_size) < 0) {
                perror("ioctl(TIOCGWINSZ)");
            } else {
                handle_resize(console_size.ws_row, console_size.ws_col);
            }
            need_screen_resize = Q_FALSE;
        }
    }

}

/**
 * Open a file in the working directory.  It will be opened in "a" mode
 * (opened for appending, created if it does not exist).  This is used for
 * capture file, log file, screen/scrollback dump, and phonebook save files.
 *
 * @param filename the filename to open.  It can be a relative or absolute
 * path.  If absolute, then new_filename will point to a strdup()d copy of
 * filename.
 * @param new_filename this will point to a newly-allocated string containing
 * the full pathname of the opened file, usually
 * /home/username/.lcxterm/filename.
 * @return the opened file handle
 */
FILE * open_workingdir_file(const char * filename, char ** new_filename) {

    *new_filename = NULL;
    int length = strlen(filename);

    if (strlen(filename) == 0) {
        return NULL;
    }

    if (filename[0] != '/') {

        /* Relative path, prefix working directory */
        *new_filename = (char *)Xmalloc(length +
            strlen(get_option(Q_OPTION_WORKING_DIR)) + 2, __FILE__, __LINE__);
        memset(*new_filename, 0, strlen(filename) +
            strlen(get_option(Q_OPTION_WORKING_DIR)) + 2);

        strncpy(*new_filename, get_option(Q_OPTION_WORKING_DIR),
            strlen(get_option(Q_OPTION_WORKING_DIR)));
        (*new_filename)[strlen(*new_filename)] = '/';
        strncpy(*new_filename + strlen(*new_filename), filename,
            length);
    } else {
        /* Duplicate the passed-in filename */
        *new_filename = Xstrdup(filename, __FILE__, __LINE__);
    }

    return fopen(*new_filename, "a");
}


/**
 * Get the full path to a filename in the data directory.  Note that the
 * string returned is a single static buffer, i.e. this is NOT thread-safe.
 *
 * @param filename a relative filename
 * @return the full path to the filename (usually ~/lcxterm/filename).
 */
char * get_datadir_filename(const char * filename) {
    assert(q_home_directory != NULL);
    sprintf(datadir_filename, "%s/%s", q_home_directory, filename);
    return datadir_filename;
}

/**
 * Get the full path to a filename in the working directory.  Note that the
 * string returned is a single static buffer, i.e. this is NOT thread-safe.
 *
 * @param filename a relative filename
 * @return the full path to the filename (usually ~/.lcxterm/filename).
 */
char * get_workingdir_filename(const char * filename) {
    sprintf(datadir_filename, "%s/%s",
        get_option(Q_OPTION_WORKING_DIR), filename);

    return datadir_filename;
}

/**
 * Open a file in the data directory.
 *
 * @param filename the filename to open.  It can be a relative or absolute
 * path.  If absolute, then new_filename will point to a strdup()d copy of
 * filename.
 * @param new_filename this will point to a newly-allocated string containing
 * the full pathname of the opened file, usually
 * /home/username/lcxterm/filename.
 * @param mode the fopen mode to use
 * @return the opened file handle
 */
FILE * open_datadir_file(const char * filename, char ** new_filename,
                         const char * mode) {

    int length = strlen(filename);
    int homedir_length = strlen(q_home_directory);

    assert(q_home_directory != NULL);

    *new_filename = NULL;

    if (strlen(filename) == 0) {
        return NULL;
    }

    if (filename[0] != '/') {
        /* Relative path, prefix data directory */
        *new_filename = (char *)Xmalloc(strlen(filename) +
            strlen(q_home_directory) + 2, __FILE__, __LINE__);
        memset(*new_filename, 0, strlen(filename) +
            strlen(q_home_directory) + 2);

        strncpy(*new_filename, q_home_directory, homedir_length);
        (*new_filename)[strlen(*new_filename)] = '/';

        /*
         * GCC does not like the way strncpy uses filename's length.  But it
         * is safe here because new_filename was reallocated to have enough
         * space for it.
         */
#pragma GCC diagnostic ignored "-Wstringop-overflow"

        strncpy(*new_filename + strlen(*new_filename), filename, length);
    } else {
        /* Duplicate the passed-in filename */
        *new_filename = Xstrdup(filename, __FILE__, __LINE__);
    }

    return fopen(*new_filename, mode);
}

/**
 * Spawn a command in an external terminal.  This is used for the mail reader
 * and external file editors.
 *
 * @param command the command line to execute
 */
void spawn_terminal(const char * command) {
    reset_shell_mode();
    system(command);
    reset_prog_mode();
    screen_really_clear();
    q_screen_dirty = Q_TRUE;
}

/**
 * Reset the global status and variables to their default state.
 */
static void reset_global_state() {
    struct winsize console_size;

    /* Initial program state */
    q_program_state = Q_STATE_INITIALIZATION;

    /*
     * Read only flag.  When set, things that can write to disk are disabled.
     */
    q_status.read_only              = Q_FALSE;

    /*
     * Passthru flag.  When set, ncurses is never initialized or used.
     */
    q_status.passthru               = Q_FALSE;

    /* Default to XTERM as the most common denominator. */
    q_status.emulation              = Q_EMUL_XTERM_UTF8;
    q_status.codepage               = default_codepage(q_status.emulation);
    /*
     * I would love to default to mixed doorway, but not sure yet what the
     * users will ultimately prefer.
     */
    q_status.doorway_mode           = Q_DOORWAY_MODE_OFF;

#ifndef Q_NO_ZMODEM
    q_status.zmodem_autostart       = Q_TRUE;
    q_status.zmodem_escape_ctrl     = Q_FALSE;
    q_status.zmodem_zchallenge      = Q_FALSE;
#endif

#ifndef Q_NO_KERMIT
    q_status.kermit_autostart               = Q_TRUE;
    q_status.kermit_robust_filename         = Q_FALSE;
    q_status.kermit_streaming               = Q_TRUE;
    q_status.kermit_long_packets            = Q_TRUE;
    q_status.kermit_uploads_force_binary    = Q_TRUE;
    q_status.kermit_downloads_convert_text  = Q_TRUE;
#endif

    q_status.xterm_double           = Q_TRUE;
    q_status.xterm_mouse_reporting  = Q_TRUE;
    q_status.vt100_color            = Q_TRUE;

    q_status.online                 = Q_FALSE;
    q_status.split_screen           = Q_FALSE;
    q_status.beeps                  = Q_FALSE;
    q_status.strip_8th_bit          = Q_FALSE;
    q_status.full_duplex            = Q_TRUE;
    q_status.line_feed_on_cr        = Q_FALSE;
    q_status.capture                = Q_FALSE;
    q_status.capture_file           = NULL;
    q_status.capture_type           = Q_CAPTURE_TYPE_NORMAL;
    q_status.screen_dump_type       = Q_CAPTURE_TYPE_NORMAL;
    q_status.scrollback_save_type   = Q_CAPTURE_TYPE_NORMAL;
    q_status.capture_x              = 0;
    q_status.scrollback_enabled     = Q_TRUE;
    q_status.scrollback_lines       = 0;
    q_status.status_visible         = Q_TRUE;
    q_status.status_line_info       = Q_FALSE;
    q_status.bracketed_paste_mode   = Q_FALSE;
    q_status.hard_backspace         = Q_TRUE;
    /*
     * Every console assumes line wrap, so turn it on by default.
     */
    q_status.line_wrap              = Q_TRUE;
    q_status.reverse_video          = Q_FALSE;
    q_status.origin_mode            = Q_FALSE;
    q_status.insert_mode            = Q_FALSE;
    q_status.led_1                  = Q_FALSE;
    q_status.led_2                  = Q_FALSE;
    q_status.led_3                  = Q_FALSE;
    q_status.led_4                  = Q_FALSE;

    /*
     * Set HEIGHT/WIDTH, in case we are passthru.
     */
    if (ioctl(STDIN_FILENO, TIOCGWINSZ, &console_size) < 0) {
        perror("ioctl(TIOCGWINSZ)");
    } else {
        HEIGHT = console_size.ws_row;
        WIDTH = console_size.ws_col;
    }

    set_status_line(Q_FALSE);
    q_status.scroll_region_top      = 0;
    q_status.scroll_region_bottom   = HEIGHT - STATUS_HEIGHT - 1;
}

/**
 * Program main entry point.
 *
 * @param argc command-line argument count
 * @param argv command-line arguments
 * @return the final program return code
 */
int lcxterm_main(int argc, char * const argv[]) {
    int option_index = 0;
    int rc;
    char * env_string;

    /* Internationalization */
    if (setlocale(LC_ALL, "") == NULL) {
        fprintf(stderr, "setlocale returned NULL: %s\n",
            strerror(errno));
        exit(EXIT_ERROR_SETLOCALE);
    }

/*
 * Bug #3528357 - The "proper" solution is to add LIBINTL to LDFLAGS and have
 * configure build the intl directory.  But until we get actual non-English
 * translations it doesn't matter.  We may as well just disable gettext().
 */
#if defined(ENABLE_NLS) && defined(HAVE_GETTEXT)
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);

    /*
    fprintf(stderr, "LANG: %s\n", getenv("LANG"));
    fprintf(stderr, "%s\n", bindtextdomain(PACKAGE, LOCALEDIR));
    fprintf(stderr, "%s\n", textdomain(PACKAGE));
     */
#endif /* ENABLE_NLS && HAVE_GETTEXT */

    /*
     * If the user asked for help or version, do that and bail out before
     * doing anything else like reading or writing to disk.
     */
    rc = check_for_help(argc, argv);
    if (rc != 0) {
        exit(rc);
    }

    /*
     * Set the global status to its defaults.
     */
    reset_global_state();

    /* Process options */
    for (;;) {
        rc = getopt_long(argc, argv, "ph?", q_getopt_long_options,
            &option_index);
        if (rc == -1) {
            /* Fall out of the for loop, we're done calling getopt_long */
            break;
        }

        /* See which new option was specified */
        switch (rc) {
        case 'p':
            q_status.passthru = Q_TRUE;
            break;
        case 0:
            process_command_line_option(
                q_getopt_long_options[option_index].name, optarg);
            break;

        default:
            break;
        }
    } /* for (;;) */

    if (q_program_state == Q_STATE_EXIT) {
        /*
         * --help or --version or somesting similar was on the command
         * line.  Bail out now.
         */
        exit(0);
    }

    /*
     * Set q_home_directory.  load_options() will create the default key
     * binding files and needs to use open_datadir_file().
     */

    if (q_dotlcxterm_dir != NULL) {
        /* The user supplied a .lcxterm directory, use that. */
        q_home_directory = q_dotlcxterm_dir;
    } else {
        /* Sustitute for $HOME */
        env_string = get_home_directory();
        q_home_directory = substitute_string("$HOME/.local/share/lcxterm",
            "$HOME", env_string);
    }

    if (q_status.passthru == Q_FALSE) {
        /*
         * Xterm: send the private sequence to select metaSendsEscape and
         * bracketed paste mode.
         */
        fprintf(stdout, "\033[?1036;2004h");
        fflush(stdout);
    }

    /* Load the options. */
    load_options();

    /* Load colors for the emulated terminal. */
    load_colors();
    q_current_color = scrollback_full_attr(Q_COLOR_CONSOLE_TEXT);

    if (q_status.passthru == Q_TRUE) {
        /* Don't bother building a long scrollback the user can't see. */
        q_status.scrollback_enabled = Q_FALSE;
    } else {
        /* Initialize curses. */
        screen_setup();
    }

    /*
     * Modify q_status based on command line options.  Do this AFTER
     * load_options() has set defaults.
     */
    resolve_command_line_options();

    if (q_status.passthru == Q_FALSE) {

        /*
         * Setup MIXED mode doorway.
         */
        setup_doorway_handling();

        /*
         * Initialize the keyboard here.  It will newterm() each supported
         * emulation, but restore things before it leaves.
         */
#ifndef Q_NO_KEYMACROS
        initialize_keyboard();
        if (q_keyfile != NULL) {
            switch_current_keyboard(q_keyfile);
        }
#endif
    }

    /*
     * See if the user wants automatic capture/logging enabled.
     */
    if (strcasecmp(get_option(Q_OPTION_CAPTURE), "true") == 0) {
        start_capture(get_option(Q_OPTION_CAPTURE_FILE));
    }

    /* Ignore SIGPIPE */
    signal(SIGPIPE, SIG_IGN);

    /* Catch SIGCHLD */
    signal(SIGCHLD, handle_sigchld);

    if (q_status.passthru == Q_TRUE) {
        /* Catch SIGWINCH */
        signal(SIGWINCH, handle_sigwinch);
    }

    if (q_program_state != Q_STATE_EXIT) {

        /*
         * Explicitly call console_refresh() so that the scrollback will be
         * set up.
         */
        console_refresh(Q_FALSE);

        /* Reset all emulations */
        reset_emulation();

        /*
         * Spawn a the local shell.
         */
        launch_shell();

        /*
         * --doorway overrides whatever launch_shell() does.
         */
        if (q_doorway_option != NULL) {
            if (strcasecmp(q_doorway_option, "doorway") == 0) {
                q_status.doorway_mode = Q_DOORWAY_MODE_FULL;
            } else if (strcasecmp(q_doorway_option, "mixed") == 0) {
                q_status.doorway_mode = Q_DOORWAY_MODE_MIXED;
            } else {
                q_status.doorway_mode = Q_DOORWAY_MODE_OFF;
            }
        }

        if (q_status.passthru == Q_TRUE) {
            /*
             * Since we don't have curses, set the terminal's fd to raw.
             */
            set_raw_termios(STDIN_FILENO);
            set_raw_termios(STDOUT_FILENO);
        }

        /* Enter main loop */
        for (;;) {
            /* Grab data */
            data_handler();

            if (q_program_state == Q_STATE_EXIT) {
                break;
            }
        }

    } /* if (q_program_state != Q_STATE_EXIT) */

    /* Close any open files */
    stop_capture();

    if (q_status.passthru == Q_TRUE) {
        /*
         * Restore the terminal to default by executing 'stty sane'.
         */
        system("stty sane");
    } else {
        /* Clear the screen */
        screen_clear();

        /* Shutdown curses */
        screen_teardown();

        /*
         * Xterm: send the private sequence to disable bracketed paste mode.
         */
        fprintf(stdout, "\033[?2004l");
        fflush(stdout);
    }

    /* Exit */
    exit(q_exitrc);

    /* Should never get here. */
    return (q_exitrc);
}

/**
 * Program main entry point.
 *
 * @param argc command-line argument count
 * @param argv command-line arguments
 * @return the final program return code
 */
int main(int argc, char * const argv[]) {
    return lcxterm_main(argc, argv);
}
