/*
 * emulation.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "common.h"

#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "screen.h"
#include "forms.h"
#include "vt100.h"
#include "keyboard.h"
#include "states.h"
#include "options.h"
#include "console.h"

/**
 * Local buffer for multiple returned characters.
 */
unsigned char q_emul_buffer[128];
int q_emul_buffer_n;
int q_emul_buffer_i;

/**
 * Last returned state.
 */
static Q_EMULATION_STATUS last_state;

/**
 * Some emulations need to wrap at special places.
 */
int q_emulation_right_margin = -1;

/**
 * The total number of bytes received on this connection.
 */
unsigned long q_connection_bytes_received;

/**
 * Avatar has a special command that requires the entire state machine be
 * re-run.  It is the responsibility of an emulation to set these two
 * variables and then return Q_EMUL_FSM_REPEAT_STATE.  terminal_emulator()
 * will free it afterwards.
 */
unsigned char * q_emul_repeat_state_buffer = NULL;
int q_emul_repeat_state_count;

/**
 * Given an emulation string, return a Q_EMULATION enum.
 *
 * @param string "TTY", "VT100", etc.  Note string is case-sensitive.
 * @return Q_EMUL_TTY, Q_EMUL_VT100, etc.
 */
Q_EMULATION emulation_from_string(const char * string) {

    if (strncasecmp(string, "VT100", sizeof("VT100")) == 0) {
        return Q_EMUL_VT100;
    } else if (strncasecmp(string, "VT102", sizeof("VT102")) == 0) {
        return Q_EMUL_VT102;
    } else if (strncasecmp(string, "VT220", sizeof("VT220")) == 0) {
        return Q_EMUL_VT220;
    } else if (strncasecmp(string, "L_8BIT", sizeof("L_8BIT")) == 0) {
        return Q_EMUL_LINUX;
    } else if (strncasecmp(string, "LINUX", sizeof("LINUX")) == 0) {
        return Q_EMUL_LINUX_UTF8;
    } else if (strncasecmp(string, "X_8BIT", sizeof("X_8BIT")) == 0) {
        return Q_EMUL_XTERM;
    } else if (strncasecmp(string, "XTERM", sizeof("XTERM")) == 0) {
        return Q_EMUL_XTERM_UTF8;
    }

    return Q_EMUL_VT102;
}

/**
 * Return a string for a Q_EMULATION enum.
 *
 * @param emulation Q_EMUL_TTY etc.
 * @return "TTY" etc.
 */
const char * emulation_string(const Q_EMULATION emulation) {

    switch (emulation) {

    case Q_EMUL_VT100:
        return "VT100";

    case Q_EMUL_VT102:
        return "VT102";

    case Q_EMUL_VT220:
        return "VT220";

    case Q_EMUL_LINUX:
        return "L_8BIT";

    case Q_EMUL_LINUX_UTF8:
        return "LINUX";

    case Q_EMUL_XTERM:
        return "X_8BIT";

    case Q_EMUL_XTERM_UTF8:
        return "XTERM";

    }

    /*
     * Should never get here.
     */
    abort();
    return NULL;
}

/**
 * Get the appropriate TERM environment variable value for an emulation.
 *
 * @param emulation the emulation
 * @return "ansi", "xterm", etc.
 */
const char * emulation_term(Q_EMULATION emulation) {
    switch (emulation) {
    case Q_EMUL_VT100:
        return "vt100";
    case Q_EMUL_VT102:
        return "vt102";
    case Q_EMUL_VT220:
        return "vt220";
    case Q_EMUL_LINUX:
    case Q_EMUL_LINUX_UTF8:
        return "linux";
    case Q_EMUL_XTERM:
    case Q_EMUL_XTERM_UTF8:
        return "xterm";
    default:
        /*
         * No default terminal setting
         */
        return "";
    }
}

/**
 * Get the appropriate LANG environment variable value for an emulation.
 *
 * @param emulation the emulation
 * @return "en", "en_US", etc.
 */
const char * emulation_lang(Q_EMULATION emulation) {
    switch (emulation) {
    case Q_EMUL_XTERM_UTF8:
    case Q_EMUL_LINUX_UTF8:
        return get_option(Q_OPTION_UTF8_LANG);
    default:
        return get_option(Q_OPTION_ISO8859_LANG);
    }
}

/**
 * Process a control character.  This is used by ANSI, AVATAR, and TTY.
 *
 * @param control_char a byte in the C0 or C1 range.
 */
void generic_handle_control_char(const unsigned char control_char) {

    /*
     * Handle control characters
     */
    switch (control_char) {
    case 0x05:
        /*
         * ENQ - transmit the answerback message.
         */
        qodem_write(q_child_tty_fd, get_option(Q_OPTION_ENQ_ANSWERBACK),
                    strlen(get_option(Q_OPTION_ENQ_ANSWERBACK)), Q_TRUE);
        break;

    case 0x07:
        /*
         * BEL
         */
        screen_beep();
        break;

    case 0x08:
        /*
         * BS
         */
        cursor_left(1, Q_FALSE);
        break;

    case 0x09:
        /*
         * HT
         */
        while (q_status.cursor_x < 80) {
            print_character(' ');
            if (q_status.cursor_x % 8 == 0) {
                break;
            }
        }
        break;

    case 0x0A:
        /*
         * LF
         */
        cursor_linefeed(Q_FALSE);
        break;

    case 0x0B:
        /*
         * VT
         */
        cursor_linefeed(Q_FALSE);
        break;

    case 0x0C:
        /*
         * FF
         *
         * In VT100 land form feed is the same as vertical tab.
         *
         * In PC-DOS land form feed clears the screen and homes the cursor.
         */
        cursor_formfeed();
        break;

    case 0x0D:
        /*
         * CR
         */
        cursor_carriage_return();
        break;

    case 0x0E:
        /*
         * SO
         *
         * Fall through...
         */
    case 0x0F:
        /*
         * SI
         *
         * Fall through...
         */
    default:
        /*
         * This is probably a CP437 glyph.
         */
        print_character(cp437_chars[control_char]);
        break;
    }
}

/**
 * Get the default 8-bit codepage for an emulation.  This is usually CP437 or
 * DEC.
 *
 * @param emulation Q_EMUL_TTY, Q_EMUL_ANSI, etc.
 * @return the codepage
 */
Q_CODEPAGE default_codepage(Q_EMULATION emulation) {
    /*
     * Set the right codepage
     */
    switch (emulation) {
    case Q_EMUL_VT100:
    case Q_EMUL_VT102:
    case Q_EMUL_VT220:
    case Q_EMUL_LINUX_UTF8:
    case Q_EMUL_XTERM_UTF8:
        return Q_CODEPAGE_DEC;
    case Q_EMUL_LINUX:
    case Q_EMUL_XTERM:
        return Q_CODEPAGE_CP437;
    }

    /*
     * BUG: should never get here
     */
    abort();
    return Q_CODEPAGE_CP437;
}

/* The main entry point for all terminal emulation -------------------------- */

/**
 * All the emulations use the same top-level function.  Q_EMULATION_STATE is
 * returned.
 *
 * If Q_EMUL_FSM_NO_CHAR_YET, then to_screen contains the number of
 * characters that can be safely discarded from the data stream, usually 1.
 *
 * If Q_EMUL_FSM_ONE_CHAR, then to_screen contains one character that can be
 * rendered.
 *
 * If Q_EMUL_FSM_MANY_CHARS, then to_screen contains one character that can
 * be rendered, AND more characters are ready.  Continue calling
 * terminal_emulator() until Q_EMUL_FSM_NO_CHAR_YET is returned.
 *
 * The emulator is expected to modify the following globals:
 *        q_current_color
 *        q_status.cursor_x
 *        q_status.cursor_y
 *        q_status.scroll_region_top
 *        q_status.scroll_region_bottom
 *
 * Also the emulator may modify data in the scrollback buffer.
 *
 * @param from_modem one byte from the remote side.
 * @param to_screen if the return is Q_EMUL_FSM_ONE_CHAR or
 * Q_EMUL_FSM_MANY_CHARS, then to_screen will have a character to display on
 * the screen.
 * @return one of the Q_EMULATION_STATUS constants.
 */
Q_EMULATION_STATUS terminal_emulator(const unsigned char from_modem,
                                     wchar_t * to_screen) {

    int i;

    /*
     * Junk extraneous data
     */
    if (q_emul_buffer_n >= sizeof(q_emul_buffer) - 1) {
        q_emul_buffer_n = 0;
        q_emul_buffer_i = 0;
        memset(q_emul_buffer, 0, sizeof(q_emul_buffer));
        last_state = Q_EMUL_FSM_NO_CHAR_YET;
    }

    if (last_state == Q_EMUL_FSM_MANY_CHARS) {
        /*
         * Everybody else just dumps the string in q_emul_buffer
         */
        if (q_emul_buffer_n == 0) {
            /*
             * We just emitted the last character
             */
            last_state = Q_EMUL_FSM_NO_CHAR_YET;
            *to_screen = 0;
            return last_state;
        }

        *to_screen = codepage_map_char(q_emul_buffer[q_emul_buffer_i]);
        q_emul_buffer_i++;
        if (q_emul_buffer_i == q_emul_buffer_n) {
            /*
             * This is the last character
             */
            q_emul_buffer_n = 0;
            q_emul_buffer_i = 0;
            memset(q_emul_buffer, 0, sizeof(q_emul_buffer));
        }
        return Q_EMUL_FSM_MANY_CHARS;
    }

    /*
     * A new character has arrived.  Increase the byte counter.
     */
    q_connection_bytes_received++;

    /*
     * VT100 scrolling regions require that the vt100() function sees these
     * characters.
     */
    if ((q_status.emulation != Q_EMUL_VT100) &&
        (q_status.emulation != Q_EMUL_VT102) &&
        (q_status.emulation != Q_EMUL_VT220) &&
        (q_status.emulation != Q_EMUL_LINUX) &&
        (q_status.emulation != Q_EMUL_LINUX_UTF8) &&
        (q_status.emulation != Q_EMUL_XTERM) &&
        (q_status.emulation != Q_EMUL_XTERM_UTF8)
    ) {
        if (from_modem == C_CR) {
            cursor_carriage_return();
            *to_screen = 1;
            return Q_EMUL_FSM_NO_CHAR_YET;
        } else if (from_modem == C_LF) {
            cursor_linefeed(Q_FALSE);
            *to_screen = 1;
            return Q_EMUL_FSM_NO_CHAR_YET;
        }
    }

    /*
     * Dispatch to the specific emulation function.
     */
    switch (q_status.emulation) {
    case Q_EMUL_VT100:
    case Q_EMUL_VT102:
    case Q_EMUL_VT220:
    case Q_EMUL_LINUX:
    case Q_EMUL_LINUX_UTF8:
    case Q_EMUL_XTERM:
    case Q_EMUL_XTERM_UTF8:
        last_state = vt100(from_modem, to_screen);
        break;
    }

    if (last_state == Q_EMUL_FSM_REPEAT_STATE) {

        for (i = 0; i < q_emul_repeat_state_count; i++) {

            switch (q_status.emulation) {
            case Q_EMUL_VT100:
            case Q_EMUL_VT102:
            case Q_EMUL_VT220:
            case Q_EMUL_LINUX:
            case Q_EMUL_LINUX_UTF8:
            case Q_EMUL_XTERM:
            case Q_EMUL_XTERM_UTF8:
                last_state = vt100(q_emul_repeat_state_buffer[i], to_screen);
                break;
            }

            /*
             * Ugly hack, this should be console
             */
            if (last_state == Q_EMUL_FSM_ONE_CHAR) {
                /*
                 * Print this character
                 */
                print_character(codepage_map_char((unsigned char) *to_screen));
            }

        }

        Xfree(q_emul_repeat_state_buffer, __FILE__, __LINE__);
        q_emul_repeat_state_buffer = NULL;

        *to_screen = 1;
        last_state = Q_EMUL_FSM_NO_CHAR_YET;
    }

    return last_state;
}

/**
 * Reset the emulation state.
 */
void reset_emulation() {
    q_emul_buffer_n = 0;
    q_emul_buffer_i = 0;
    memset(q_emul_buffer, 0, sizeof(q_emul_buffer));
    last_state = Q_EMUL_FSM_NO_CHAR_YET;

    if (q_emul_repeat_state_buffer != NULL) {
        Xfree(q_emul_repeat_state_buffer, __FILE__, __LINE__);
        q_emul_repeat_state_buffer = NULL;
    }

    q_current_color = Q_A_NORMAL | scrollback_full_attr(Q_COLOR_CONSOLE_TEXT);
#ifdef Q_ENABLE_24BITRGB
    q_current_color_fg_rgb = -1;
    q_current_color_bg_rgb = -1;
#endif

    /*
     * Reset ALL of the emulators
     */
    vt100_reset();
    q_emulation_right_margin = -1;
    q_status.scroll_region_top = 0;
    q_status.scroll_region_bottom = HEIGHT - STATUS_HEIGHT - 1;
    q_status.reverse_video = Q_FALSE;
    q_status.origin_mode = Q_FALSE;

    switch (q_status.emulation) {
    case Q_EMUL_LINUX:
    case Q_EMUL_LINUX_UTF8:
        /*
         * LINUX emulation specifies that backspace is DEL.
         */
        q_status.hard_backspace = Q_FALSE;
        break;

    case Q_EMUL_VT220:
    case Q_EMUL_XTERM:
    case Q_EMUL_XTERM_UTF8:
        /*
         * VT220 style emulations tend to use DEL
         */
        q_status.hard_backspace = Q_FALSE;
        break;

    case Q_EMUL_VT100:
    case Q_EMUL_VT102:
        q_status.hard_backspace = Q_TRUE;
        break;
    }

    /*
     * If xterm mouse reporting is enabled, enable the mouse.  Do not resolve
     * double and triple clicks.
     */
    if ((q_status.xterm_mouse_reporting == Q_TRUE) &&
        ((q_status.emulation == Q_EMUL_XTERM) ||
         (q_status.emulation == Q_EMUL_XTERM_UTF8))
    ) {
        /*
         * xterm emulations: listen for the mouse.
         */
        enable_mouse_listener();
    } else {
        /*
         * Non-xterm or mouse disabled, do not listen for the mouse.
         */
        disable_mouse_listener();
    }

    /*
     * Reset bracketed paste mode to global flag.
     */
    q_status.bracketed_paste_mode = Q_FALSE;
    if (strcasecmp(get_option(Q_OPTION_BRACKETED_PASTE), "true") == 0) {
        q_status.bracketed_paste_mode = Q_TRUE;
    }

}

/**
 * Draw screen for the emulation selection dialog.
 */
void emulation_menu_refresh() {
    char * status_string;
    int status_left_stop;
    char * message;
    int message_left;
    int window_left;
    int window_top;
    int window_height = 13;
    int window_length;

    if (q_screen_dirty == Q_FALSE) {
        return;
    }

    /*
     * Clear screen for when it resizes
     */
    console_refresh(Q_FALSE);

    /*
     * Put up the status line
     */
    screen_put_color_hline_yx(HEIGHT - 1, 0, cp437_chars[HATCH], WIDTH,
                              Q_COLOR_STATUS);

    status_string = _(" LETTER-Select an Emulation   ESC/`-Exit ");
    status_left_stop = WIDTH - strlen(status_string);
    if (status_left_stop <= 0) {
        status_left_stop = 0;
    } else {
        status_left_stop /= 2;
    }
    screen_put_color_str_yx(HEIGHT - 1, status_left_stop, status_string,
                            Q_COLOR_STATUS);

    window_length = 27;

    /*
     * Add room for border + 1 space on each side
     */
    window_length += 4;

    /*
     * Window will be centered on the screen
     */
    window_left = WIDTH - 1 - window_length;
    if (window_left < 0) {
        window_left = 0;
    } else {
        window_left /= 2;
    }
    window_top = HEIGHT - 1 - window_height;
    if (window_top < 0) {
        window_top = 0;
    } else {
        window_top /= 10;
    }

    /*
     * Draw the sub-window
     */
    screen_draw_box(window_left, window_top, window_left + window_length,
                    window_top + window_height);

    /*
     * Place the title
     */
    message = _("Set Emulation");
    message_left = window_length - (strlen(message) + 2);
    if (message_left < 0) {
        message_left = 0;
    } else {
        message_left /= 2;
    }
    screen_put_color_printf_yx(window_top + 0, window_left + message_left,
                               Q_COLOR_WINDOW_BORDER, " %s ", message);

    screen_put_color_str_yx(window_top + 1, window_left + 2, _("Emulation is "),
                            Q_COLOR_MENU_TEXT);
    screen_put_color_printf(Q_COLOR_MENU_COMMAND, "%s",
                            emulation_string(q_status.emulation));

    screen_put_color_str_yx(window_top + 3, window_left + 7, "A",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  VT100");
    screen_put_color_str_yx(window_top + 4, window_left + 7, "B",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  VT102");
    screen_put_color_str_yx(window_top + 5, window_left + 7, "C",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  VT220");
    screen_put_color_str_yx(window_top + 6, window_left + 7, "D",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  LINUX (8-BIT)");
    screen_put_color_str_yx(window_top + 7, window_left + 7, "E",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  LINUX (UTF-8)");
    screen_put_color_str_yx(window_top + 8, window_left + 7, "F",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  XTERM (8-BIT)");
    screen_put_color_str_yx(window_top + 9, window_left + 7, "G",
                            Q_COLOR_MENU_COMMAND);
    screen_put_color_printf(Q_COLOR_MENU_TEXT, "  XTERM (UTF-8)");

    /*
     * Prompt
     */
    screen_put_color_str_yx(window_top + 11, window_left + 2,
                            _("Your Choice ? "), Q_COLOR_MENU_COMMAND);

    screen_flush();
    q_screen_dirty = Q_FALSE;
}

/**
 * Keyboard handler for the emulation selection dialog.
 *
 * @param keystroke the keystroke from the user.
 * @param flags KEY_FLAG_ALT, KEY_FLAG_CTRL, etc.  See input.h.
 */
void emulation_menu_keyboard_handler(const int keystroke, const int flags) {
    int new_keystroke;

    /*
     * Default to xterm
     */
    Q_EMULATION new_emulation = Q_EMUL_XTERM_UTF8;

    switch (keystroke) {

    case 'A':
    case 'a':
        new_emulation = Q_EMUL_VT100;
        break;

    case 'B':
    case 'b':
        new_emulation = Q_EMUL_VT102;
        break;

    case 'C':
    case 'c':
        new_emulation = Q_EMUL_VT220;
        break;

    case 'D':
    case 'd':
        new_emulation = Q_EMUL_LINUX;
        break;

    case 'E':
    case 'e':
        new_emulation = Q_EMUL_LINUX_UTF8;
        break;

    case 'F':
    case 'f':
        new_emulation = Q_EMUL_XTERM;
        break;

    case 'G':
    case 'g':
        new_emulation = Q_EMUL_XTERM_UTF8;
        break;

    case '`':
        /*
         * Backtick works too
         */
    case Q_KEY_ESCAPE:
        /*
         * ESC return to TERMINAL mode
         */
        switch_state(Q_STATE_CONSOLE);

        /*
         * The ABORT exit point
         */
        return;

    default:
        /*
         * Ignore keystroke
         */
        return;
    }

    if (new_emulation == q_status.emulation) {
        /*
         * Ask for emulation reset
         */

        /*
         * Weird.  I had the last parameter set to "0" and GCC didn't turn it
         * into a double that equaled 0.  I have to explicitly pass 0.0 here.
         * But for some reason I didn't have this behavior in console.c ?
         */
        new_keystroke = notify_prompt_form(_("Emulation"),
                                           _("Reset Current Emulation? [y/N] "),
                                           _(" Y-Reset Emulation   N-Exit "),
                                           Q_TRUE, 0.0, "YyNn\r");
        new_keystroke = q_tolower(new_keystroke);

        /*
         * Reset only if the user said so
         */
        if (new_keystroke == 'y') {
            reset_emulation();
        }
    } else {
        q_status.emulation = new_emulation;
        reset_emulation();

#ifndef Q_NO_KEYMACROS
        /*
         * Switch the keyboard to the current emulation keyboard
         */
        switch_current_keyboard("");
#endif
    }

    /*
     * Set the right codepage
     */
    q_status.codepage = default_codepage(q_status.emulation);

    /*
     * The OK exit point
     */
    switch_state(Q_STATE_CONSOLE);
}
