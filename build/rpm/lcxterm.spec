Name:           lcxterm
Version:        0.9.1
Release:        1%{?dist}
Summary:        LCXterm - add features to the Linux console and other terminals

Group:          Applications/System
License:        Public Domain
URL:            https://gitlab.com/AutumnMeowMeow/lcxterm/
Source0:        lcxterm-0.9.1.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:  ncurses gpm
BuildRequires:  ncurses-devel gpm-devel


%description
LCXterm is a ncurses-based terminal emulator that brings additional
conveniences to the raw Linux console and other terminal emulators.
LCXterm is intended to act like an insertable component between the
normal terminal and a shell, and generally stay out of the way
otherwise until a feature is needed.

Features that work in "passthru" mode include:

    * Zmodem and Kermit downloads.

    * GPM to X10 mouse conversion for the Linux console.  This enables
      Linux console mouse events to work across remote shells.

    * Session capture to file.

When used in "non-passthru" mode, LCXterm provides all of the above
plus:

    * Xmodem, Ymodem, Zmodem, and Kermit uploads and downloads.

    * Arbitrarily long searchable scrollback, which can also be saved
      to file.  Bytes can be saved as UTF-8 text or HTML with color.

    * User-definable keyboard macros for function keys, arrow keys,
      Ins/Del/Home/End/PgUp/PgDn, etc.

    * vttest-passing terminal emulation for VT100, VT102, VT220,
      Linux, and Xterm.

    * Significantly faster processing and display.


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
cp docs/lcxterm.1 $RPM_BUILD_ROOT%{_mandir}/man1/


%post


%postun


%posttrans


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/lcxterm
%{_mandir}/man1/lcxterm.1.gz
%doc ChangeLog COPYING README.md docs/TODO.md


%changelog
* Sun Apr 22 2018 Autumn Lamonte <lamonte at, users.sourceforge.net> - 0.9-1
- Initial package creation
