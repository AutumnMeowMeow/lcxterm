LCXterm Installation
==================

LCXterm can be built in several ways.  The most common build is the
autoconf-based build described in further detail below.  Another
makefile is provided in the build/ subdirectory for the following
compilers/platforms:

```
|------------------|--------------------|----------------------------------|
| Makefile name    | Compiler           | Target Platform                  |
|------------------|--------------------|----------------------------------|
| Makefile.generic | cc                 | POSIX / Linux, text only         |
|------------------|--------------------|----------------------------------|
```

Makefiles are run from this top-level directory.  For example, to use
the Makefile.generic to produce a text-mode ncurses build:

```
$ make -f build/Makefile.generic clean
$ make -f build/Makefile.generic
```


Autoconf
--------

LCXterm is commonly installed using the GNU autoconf system.  The
following is the standard autoconf installation:

  1. Type `./configure' to configure the package for your system.

     Running `configure' takes awhile.  While running, it prints some
     messages telling which features it is checking for.

  2. Type `make' to compile the package.

  3. Type `make install' to install the LCXterm program and
     documentation.  By default, `make install' will install the
     package's files in `/usr/local/bin', `/usr/local/man', etc.  You
     can specify an installation prefix other than `/usr/local' by
     giving `configure' the option `--prefix=PATH'

  5. You can remove the LCXterm binaries and object files from the
     source code directory by typing `make clean'.  To also remove the
     files that `configure' created (so you can compile the package
     for a different kind of computer), type `make distclean'.

Note that if you build LCXterm directly from a source repository, you
must have automake installed and use that to generate Makefile.in.
You can choose to execute `./autogen.sh` and skip step 1 above (as it
already runs configure); or run `aclocal && automake && autoconf'
before step 1 above.


Debian Packages
---------------

A Debian build directory is provided in build/deb.  A 'lcxterm' deb can
be built via the following commands:

  $ cp -r build/deb/lcxterm debian
  $ dpkg-buildpackage

Note that the lcxterm tarball will need to be renamed to
lcxterm_{version}.orig.tar.gz in the parent directory.


RPM Packages
------------

An RPM spec file to create the 'lcxterm' package is provided in
build/rpm.


Pre-Processor Defines
---------------------

Available defines are listed below.  If none are set, lcxterm is built
as: do use newterm(), do have all file transfer protocols, do have
keyboard macros, do not have 24-bit RGB, do not have gpm.

  Q_NO_NEWTERM       - Do not call newterm() in initialize_keyboard().  This
                       is required for running on Arch Linux.

  Q_NO_PROTOCOLS     - Disable all file transfer protocols.

  Q_NO_XMODEM        - Disable Xmodem (and Ymodem) file transfers.

  Q_NO_ZMODEM        - Disable Zmodem file transfers.

  Q_NO_KERMIT        - Disable Zmodem file transfers.

  Q_NO_KEYMACROS     - Disable keyboard macros.

  Q_ENABLE_GPM       - Enable GPM mouse support.

  Q_ENABLE_24BITRBG  - Enable 24-bit RGB.
